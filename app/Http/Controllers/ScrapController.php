<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stock;
use App\Recommendation;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class ScrapController extends Controller
{
    
    const RECOMMENDATIONS = [
        'Market Perform' => 3,
        'Outperform' => 4,
        'Neutral' => 3,
        'Overweight' => 2,
        'Accumulate' => 4,
        'Buy' => 5,
        'Underweight' => 4,
        'Hold' => 3,
        'Top Pick' => 5,
        'Mkt Perform' => 3,
        'Underperform' => 2,
        'Positive' => 4,
        'In-line' => 3,
        'Equal Weight' => 3,
        'Sector Perform' => 3,
        'Peer Perform' => 3,
        'Sell' => 1,
        'Equal-Weight' => 3,
        'Sector Weight' => 3

    ];  
    
    public function scrap(Client $client, $action){

    $crawler = $client->request('GET', 'https://finviz.com/quote.ashx?t='.$action.'&ty=c&ta=1&p=d');

    $status = $client->getResponse()->getStatus();

    if($status == 200){

       $results = $crawler->filter('.fullview-ratings-outer > tr')->each(function (Crawler $node) {
            
            $temp = array();

             $action = $node->filter('td > table > tr')->each(function (Crawler $action) {

                
                $actionInformation = $action->filter('td');

                $actionDate = $actionInformation->eq(0)->text();
                $actionAnalyst= $actionInformation->eq(2)->text();
                $actionRecommendation = $this->convertToInteger($actionInformation->eq(3)->text());
                
                $data = [
                    'date' => $actionDate,
                    'analyst' => $actionAnalyst,
                    'value' => $actionInformation->eq(3)->text(),
                    'recommendation' => $actionRecommendation
                ];

                return $data;

            });

            return $action[0];
            
        });

        if(!empty($results)){

        $data = array_reverse(array_values(array_column(
            array_reverse($results),
            null,
            'analyst'
        )));

        $average = $this->getActionAverage($data);

        return [
            'analysts' => $data,
            'average' => $average,
            'result' => $this->convertAverageToRecommendation($average)
        ];
        
    } else {
        return [];
    }
    } else {
        return [];
    }

    }

    public function saveAnalystData(){

        //Obtener todas las acciones en la base de datos
        $stocks = Stock::all();
        $client = new Client;


        foreach ($stocks as $key => $stock) {

            $data = $this->scrap($client, $stock->slug);
            if(!empty($data)){
            $recommedation = Recommendation::create([
                'stock_id' => $stock->id,
                'value' => $data['average']
            ]);
            print_r($recommedation->html());
            }

        }

    }

    public function scrapFinancialsData(Client $client, $action){


        $crawler = $client->request('GET', 'https://www.reuters.com/companies/'.$action.'/key-metrics');

        $status = $client->getResponse()->getStatus();

        
        if($status == 200){

            $results = $crawler->filter('.KeyMetrics-container-3QO5T > div');

                try {
                //Price and Volume
                $priceAndVolume = $results->filter('.KeyMetrics-table-container-3wVZN')->first()->filter('.data > td');
                $lastBid = $priceAndVolume->eq(0)->text();
                $beta = $priceAndVolume->eq(9)->text();

               
                //Margins
                $margins = $results->filter('.KeyMetrics-table-container-3wVZN')->eq(4);
                $netProfitMargin = $margins->filter('.data > td')->eq(2)->text();

                //Per Share Data
                $perShareData = $results->filter('.KeyMetrics-table-container-3wVZN')->eq(1);
                $dividends = $perShareData->filter('.data > td')->eq(14)->text();

                //Income Statement
                $incomeStatement = $results->filter('.KeyMetrics-table-container-3wVZN')->eq(7);
                $revenue = $incomeStatement->filter('.data > td')->eq(1)->text();

                return [
                    'bid' => $lastBid,
                    'beta' => $beta,
                    'net_profit_margin' => $netProfitMargin,
                    'dividends' => $dividends,
                    'revenue' => $revenue
                ];

            } catch (\InvalidArgumentException $e) {
                
                return [
                    'bid' => 0,
                    'beta' => 0,
                    'net_profit_margin' => 0,
                    'dividends' => 0,
                    'revenue' => 0
                ];

            }

        }


    }

    public function convertToInteger($string){
        
        $string = explode("→", $string);
        $string = (count($string) == 1) ? $string[0] : ltrim($string[1], ' ');

        $VALUES = ScrapController::RECOMMENDATIONS;

        foreach (array_keys($VALUES) as $key => $value) {

            if($value == $string) return $VALUES[$value];

        }

    }

    public function getActionAverage($arr){

        $lenght = count($arr);
        $sum = 0;

        foreach ($arr as $key => $action) {
            $sum += $action['recommendation'];
        }

        return round(($sum/$lenght), 2);
    }

    public function convertAverageToRecommendation($number){

        if($number <= 1) return "Sell";
        if($number > 2 && $number < 3) return "Under-perform";
        if($number >= 3 && $number < 4) return "Hold";
        if($number >= 4 && $number < 4.5) return "Out-perform";
        if($number >= 4.5) return "Buy";
    }
}
