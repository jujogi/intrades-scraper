<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $guarded = [];

    public function recommendations(){
        return $this->hasMany(Recommendation::class)->orderBy('created_at', 'asc');;
    }
}
